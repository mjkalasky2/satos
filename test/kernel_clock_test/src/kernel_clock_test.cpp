#include "satos/kernel.h"
#include "satos/tester.h"
#include "satos/formatters/formatters.h"
#include "satext/units.h"

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;
using time_point = satos::clock::time_point;
using duration = satos::clock::duration;

int main(int argv, char** argc) {
    start_test("kernel_clock_test", argv, argc);
    kernel::set_idle_handler([]() {
        static std::uint32_t counter = 0;
        static std::uint32_t tick_duration = clock::to_ticks(1_s) / kernel::get_tick_freq();

        // test if clock is counting
        assert_that(clock::now(), is_eq(time_point(counter * duration(clock::tick_duration_ms()))));

        assert_that(kernel::get_tick_count(), is_eq(counter * tick_duration));

        counter++;
        asm("wfi");
        if (counter > 10) {
            end_test();
        }
    });

    // test if clock isn't started before kernel start
    assert_that(kernel::get_tick_count(), is_eq(0));
    assert_that(clock::now(), is_eq(time_point(0_ms)));

    kernel::initialize(50_MHz, 100_Hz);

    // test if tick frequency was properly set
    assert_that(kernel::get_tick_freq(), is_eq(100_Hz));

    // test if tick duration was properly set
    assert_that(clock::tick_duration_ms(), is_eq(10));

    kernel::start();
    return 0;
}
