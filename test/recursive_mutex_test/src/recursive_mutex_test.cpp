#include "satos/kernel.h"
#include "satos/thread.h"
#include "satos/mutex.h"
#include "satos/tester.h"
#include "satos/formatters/formatters.h"
#include "satext/units.h"

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;
using time_point = satos::clock::time_point;

class high_prio_thread;
using high_prio_thread_base = thread<high_prio_thread, thread_priority::high_0>;
class high_prio_thread : public high_prio_thread_base {
    friend high_prio_thread_base;

public:
    high_prio_thread(recursive_mutex& mtx) : mtx_(mtx) {}

private:
    void thread_function() {
        { // 1. multiple lock and unlock test
            mtx_.lock();
            mtx_.lock();
            this_thread::sleep_for(50_ms);
            mtx_.unlock();
            mtx_.unlock();
        }

        this_thread::sleep_for(10_ms); // switch context to lower prio thread

        { // 2. try_lock_for failure and multiple success test
            auto result = mtx_.try_lock_for(40_ms);
            assert_that(result, is_eq(false));

            result = mtx_.try_lock_for(100_ms);
            assert_that(result, is_eq(true));

            assert_that(clock::now(), is_eq(time_point(150_ms)));

            result = mtx_.try_lock();
            assert_that(result, is_eq(true));

            this_thread::sleep_for(10_ms); // switch context to lower prio thread

            mtx_.unlock();
            mtx_.unlock();
        }

        assert_that(clock::now(), is_eq(time_point(160_ms)));
        this_thread::sleep_for(40_ms); // switch context to lower prio thread

        { // 3. try_lock_until failure and multiple success test
            auto result = mtx_.try_lock_until(time_point(240_ms));
            assert_that(result, is_eq(false));

            assert_that(clock::now(), is_eq(time_point(240_ms)));

            result = mtx_.try_lock_until(time_point(300_ms));
            assert_that(result, is_eq(true));
            assert_that(clock::now(), is_eq(time_point(270_ms)));

            result = mtx_.try_lock();
            assert_that(result, is_eq(true));
        }

        end_test();
    }

    recursive_mutex& mtx_;
};

class low_prio_thread;
using low_prio_thread_base = thread<low_prio_thread>;
class low_prio_thread : public low_prio_thread_base {
    friend low_prio_thread_base;

public:
    low_prio_thread(recursive_mutex& mtx) : mtx_(mtx) {}

private:
    void thread_function() {
        { // 2. try_lock_for failure and multiple success test
            mtx_.lock();
            assert_that(clock::now(), is_eq(time_point(50_ms)));

            this_thread::sleep_for(100_ms);
            mtx_.unlock();

            assert_that(clock::now(), is_eq(time_point(150_ms)));
            auto result = mtx_.try_lock();
            assert_that(result, is_eq(false));

            this_thread::sleep_for(20_ms); // switch context to higher prio thread
        }

        { // 3. try_lock_for failure and multiple success test
            mtx_.lock();
            assert_that(clock::now(), is_eq(time_point(170_ms)));

            this_thread::sleep_for(100_ms);
            mtx_.unlock();
        }
    }

    recursive_mutex& mtx_;
};

int main(int argv, char** argc) {
    start_test("recursive_mutex_test", argv, argc);

    static recursive_mutex mtx;
    static high_prio_thread high(mtx);
    static low_prio_thread low(mtx);

    assert_that(mtx.native_handle(), is_not_eq(static_cast<void*>(nullptr)));

    kernel::initialize(50_MHz, 100_Hz);

    high.start();
    low.start();

    kernel::start();
    return 0;
}
