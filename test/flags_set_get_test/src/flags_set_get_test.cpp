#include "satos/kernel.h"
#include "satos/thread.h"
#include "satos/flags.h"
#include "satos/tester.h"
#include "satos/formatters/formatters.h"
#include "satext/units.h"

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;
using time_point = satos::clock::time_point;

class flags_thread;
using flags_thread_base = thread<flags_thread, thread_priority::high_0>;
class flags_thread : public flags_thread_base {
    friend flags_thread_base;

public:
    flags_thread() {}

private:
    void thread_function() {
        auto result = flags_.get();
        assert_that(result.has_value(), is_eq(true));
        assert_that(*result, is_eq(0x0));

        result = flags_.set(0xff);
        assert_that(result.has_value(), is_eq(true));
        assert_that(*result, is_eq(0xff));

        result = flags_.get();
        assert_that(result.has_value(), is_eq(true));
        assert_that(*result, is_eq(0xff));

        result = flags_.set(0xff0000);
        assert_that(result.has_value(), is_eq(true));
        assert_that(*result, is_eq(0xff00ff));

        result = flags_.get();
        assert_that(result.has_value(), is_eq(true));
        assert_that(*result, is_eq(0xff00ff));

        result = flags_.clear(0xff);
        assert_that(result.has_value(), is_eq(true));
        assert_that(*result, is_eq(0xff00ff));

        result = flags_.get();
        assert_that(result.has_value(), is_eq(true));
        assert_that(*result, is_eq(0xff0000));

        result = flags_.clear(0xffffff);
        assert_that(result.has_value(), is_eq(true));
        assert_that(*result, is_eq(0xff0000));

        result = flags_.get();
        assert_that(result.has_value(), is_eq(true));
        assert_that(*result, is_eq(0x0));

        end_test();
    }

    flags flags_;
};

int main(int argv, char** argc) {
    start_test("flags_set_get_test", argv, argc);

    static flags_thread t;

    kernel::initialize(50_MHz, 100_Hz);

    t.start();

    kernel::start();
    return 0;
}
