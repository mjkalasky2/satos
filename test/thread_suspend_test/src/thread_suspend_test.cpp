#include "satos/kernel.h"
#include "satos/thread.h"
#include "satos/tester.h"
#include "satos/formatters/formatters.h"
#include "satext/units.h"

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;
using time_point = satos::clock::time_point;

class suspended_thread;
using suspended_thread_base = thread<suspended_thread, thread_priority::high_0>;
class suspended_thread : public suspended_thread_base {
    friend suspended_thread_base;

private:
    void thread_function() {
        { // test if thread is started at 0 ms
            assert_that(clock::now(), is_eq(time_point(0_ms)));
        }

        { // test if suspended thread wont run after 10 ms, but after 20 ms when it will be
          // resumed
            this_thread::sleep_for(10_ms);
            assert_that(clock::now(), is_eq(time_point(20_ms)));
        }

        end_test();
    }
};

class test_thread;
using test_thread_base = thread<test_thread>;
class test_thread : public test_thread_base {
    friend test_thread_base;

public:
    explicit test_thread(suspended_thread& thread) : thread_(thread) {}

private:
    void thread_function() {
        { // test if another thread is in blocked state
            assert_that(thread_.get_state(), is_eq(thread_state::blocked));
        }

        { // test suspend and resume of another thread
            thread_.suspend();
            this_thread::sleep_for(20_ms);
            thread_.resume();
        }
    }

    suspended_thread& thread_;
};

int main(int argv, char** argc) {
    start_test("thread_suspend_test", argv, argc);
    static suspended_thread s;
    static test_thread t(s);

    kernel::initialize(50_MHz, 100_Hz);
    t.start();
    s.start();
    kernel::start();
    return 0;
}
