#include "satos/kernel.h"
#include "satos/thread.h"
#include "satos/flags.h"
#include "satos/tester.h"
#include "satos/formatters/formatters.h"
#include "satext/units.h"

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;
using time_point = satos::clock::time_point;

class high_prio_thread;
using high_prio_thread_base = thread<high_prio_thread, thread_priority::high_0>;
class high_prio_thread : public high_prio_thread_base {
    friend high_prio_thread_base;

public:
    high_prio_thread(flags& f) : flags_(f) {}

private:
    void thread_function() {
        { // 1. wait/set test w/o clear
            auto result = flags_.wait(0x01, false);
            assert_that(clock::now(), is_eq(time_point(50_ms)));
            assert_that(result.has_value(), is_eq(true));
            assert_that(*result, is_eq(0x01));

            result = flags_.get();
            assert_that(result.has_value(), is_eq(true));
            assert_that(*result, is_eq(0x1));

            flags_.clear(0xffffff);
        }

        { // 2. wait/set test w/ clear
            auto result = flags_.wait(0x01, true);
            assert_that(clock::now(), is_eq(time_point(100_ms)));
            assert_that(result.has_value(), is_eq(true));
            assert_that(*result, is_eq(0x01));

            result = flags_.get();
            assert_that(result.has_value(), is_eq(true));
            assert_that(*result, is_eq(0x0));
        }

        { // 3. wait/set test w/ clear timeout
            auto result = flags_.wait(0x01, true, 20_ms);
            assert_that(clock::now(), is_eq(time_point(120_ms)));
            assert_that(result.has_value(), is_eq(false));
        }

        { // 4. wait/set multiple bits test
            auto result = flags_.wait(0xff, true);
            assert_that(clock::now(), is_eq(time_point(150_ms)));
            assert_that(result.has_value(), is_eq(true));
            assert_that(*result, is_eq(0x80));

            flags_.clear(0xffffff);
        }

        { // 5. wait for all/set test
            auto result = flags_.wait_for_all(0xff, true);
            assert_that(clock::now(), is_eq(time_point(200_ms)));
            assert_that(result.has_value(), is_eq(true));
            assert_that(*result, is_eq(0xff));
        }

        { // 6. wait for all/set test single bit
            auto result = flags_.wait_for_all(0xff, true, 70_ms);
            assert_that(clock::now(), is_eq(time_point(270_ms)));
            assert_that(result.has_value(), is_eq(false));

            result = flags_.get();
            assert_that(result.has_value(), is_eq(true));
            assert_that(*result, is_eq(0x01));

            flags_.clear(0xffffff);
        }

        { // 7. wait w/ abs timeout
            auto result = flags_.wait(0x01, true, time_point{350_ms});
            assert_that(clock::now(), is_eq(time_point(330_ms)));
            assert_that(result.has_value(), is_eq(true));
            assert_that(*result, is_eq(0x01));

            flags_.clear(0xffffff);
        }

        { // 8. wait w/ abs timeout
            auto result = flags_.wait(0x01, true, time_point{400_ms});
            assert_that(clock::now(), is_eq(time_point(400_ms)));
            assert_that(result.has_value(), is_eq(false));

            flags_.clear(0xffffff);
        }

        end_test();
    }

    flags& flags_;
};

class low_prio_thread;
using low_prio_thread_base = thread<low_prio_thread>;
class low_prio_thread : public low_prio_thread_base {
    friend low_prio_thread_base;

public:
    low_prio_thread(flags& f) : flags_(f) {}

private:
    void thread_function() {
        { // 1. wait/set test w/o clear
            this_thread::sleep_for(50_ms);
            flags_.set(0x01);
        }

        { // 2. wait/set test w/ clear
            this_thread::sleep_for(50_ms);
            flags_.set(0x01);
        }

        { // 4. wait/set multiple bits test
            this_thread::sleep_for(50_ms);
            flags_.set(0x80);
        }

        { // 5. wait/set multiple bits test
            this_thread::sleep_for(50_ms);
            flags_.set(0xff);
        }

        { // 6. wait for all/set test single bit
            this_thread::sleep_for(50_ms);
            flags_.set(0x01);
        }

        { // 7. wait w/ abs timeout
            this_thread::sleep_for(80_ms);
            flags_.set(0x01);
        }
    }

    flags& flags_;
};

int main(int argv, char** argc) {
    start_test("flags_test", argv, argc);

    static flags f;
    static high_prio_thread high(f);
    static low_prio_thread low(f);

    kernel::initialize(50_MHz, 100_Hz);

    high.start();
    low.start();

    kernel::start();
    return 0;
}
