#pragma once
#include <string_view>
#include <concepts>
#include <experimental/source_location>
#include <fmt/format.h>

namespace satos_tester {
namespace detail {
class tester {
public:
    static tester& instance();

    void start();
    bool is_started();
    std::string_view get_name();
    void set_name(std::string_view name);
    void set_assert_failed();
    bool get_assert_failed();

private:
    tester() = default;
    bool started_;
    std::string_view name_;
    bool assert_failed_{false};
};
} // namespace detail

void end_test();

void start_test(std::string_view name, int argc, char** argv);

// clang-format off
template<typename T, typename U>
concept comparision_operator = requires(T a, U b) {
    { a(b) } -> std::same_as<bool>;
    { a.value() } -> std::convertible_to<const U&>;
};
// clang-format on

template<typename T>
class is_eq {
public:
    explicit is_eq(const T& value) : value_(value) {}

    const T& value() const { return value_; };

    bool operator()(const T& value) const { return value == value_; }

private:
    const T& value_;
};

template<typename T>
class is_not_eq {
public:
    explicit is_not_eq(const T& value) : value_(value) {}

    const T& value() const { return value_; };

    bool operator()(const T& value) const { return value != value_; }

private:
    const T& value_;
};

template<typename T, comparision_operator<T> C>
void assert_that(const T& value,
                 const C& expected,
                 const std::experimental::source_location location =
                     std::experimental::source_location::current()) {
    if (!detail::tester::instance().is_started()) {
        return;
    }
    if (!expected(value)) {
        fmt::print("{}:{}\n", location.file_name(), location.line());
        fmt::print("    Got: {}\n", value);
        fmt::print("    Expected: {}\n", expected.value());

        detail::tester::instance().set_assert_failed();
        end_test();
    }
}

template<typename T1, typename T2>
void assert_eq(const T1& value, const T2& expected) {
    if (!detail::tester::instance().is_started()) {
        return;
    }
    if (value != expected) {
        detail::tester::instance().set_assert_failed();
        end_test();
    }
}
} // namespace satos_tester
