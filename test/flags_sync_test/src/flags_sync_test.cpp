#include "satos/kernel.h"
#include "satos/thread.h"
#include "satos/flags.h"
#include "satos/tester.h"
#include "satos/formatters/formatters.h"
#include "satext/units.h"

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;
using time_point = satos::clock::time_point;

class high_prio_thread;
using high_prio_thread_base = thread<high_prio_thread, thread_priority::high_0>;
class high_prio_thread : public high_prio_thread_base {
    friend high_prio_thread_base;

public:
    high_prio_thread(flags& f) : flags_(f) {}

private:
    void thread_function() {
        { // 1. sync test
            auto result = flags_.sync(0x01, 0x10);
            assert_that(clock::now(), is_eq(time_point(50_ms)));
            assert_that(result.has_value(), is_eq(true));
            assert_that(*result, is_eq(0x11));
        }

        {
            auto result = flags_.sync(0x0100, 0x1000, 20_ms);
            assert_that(clock::now(), is_eq(time_point(70_ms)));
            assert_that(result.has_value(), is_eq(false));
        }

        end_test();
    }

    flags& flags_;
};

class low_prio_thread;
using low_prio_thread_base = thread<low_prio_thread>;
class low_prio_thread : public low_prio_thread_base {
    friend low_prio_thread_base;

public:
    low_prio_thread(flags& f) : flags_(f) {}

private:
    void thread_function() {
        { // 1. sync test
            this_thread::sleep_for(50_ms);
            flags_.sync(0x10, 0x01);
        }
    }

    flags& flags_;
};

int main(int argv, char** argc) {
    start_test("flags_sync_test", argv, argc);

    static flags f;
    static high_prio_thread high(f);
    static low_prio_thread low(f);

    kernel::initialize(50_MHz, 100_Hz);

    high.start();
    low.start();

    kernel::start();
    return 0;
}
