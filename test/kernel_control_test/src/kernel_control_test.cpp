#include "satos/kernel.h"
#include "satos/tester.h"
#include "satos/formatters/formatters.h"
#include "satext/units.h"

using namespace satos;
using namespace satos_tester;

int main(int argv, char** argc) {
    start_test("kernel_control", argv, argc);
    kernel::set_idle_handler([]() {
        // check if kernel is running
        assert_that(kernel::get_state(), is_eq(kernel::state::running));

        // check if kernel is locked
        kernel::lock();
        assert_that(kernel::get_state(), is_eq(kernel::state::locked));

        // check if kernel is unlocked
        kernel::unlock();
        assert_that(kernel::get_state(), is_eq(kernel::state::running));

        end_test();
    });

    // check state before initializing kernel
    assert_that(kernel::get_state(), is_eq(kernel::state::inactive));

    // check state after initializing kernel
    kernel::initialize(50_MHz, 100_Hz);
    assert_that(kernel::get_state(), is_eq(kernel::state::ready));

    kernel::start();
    return 0;
}
