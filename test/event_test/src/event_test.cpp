#include "satos/kernel.h"
#include "satos/thread.h"
#include "satos/event.h"
#include "satos/tester.h"
#include "satos/formatters/formatters.h"
#include "satext/units.h"

using namespace satos;
using namespace satos_tester;
using namespace satos::chrono_literals;
using time_point = satos::clock::time_point;

class high_prio_thread;
using high_prio_thread_base = thread<high_prio_thread, thread_priority::high_0>;
class high_prio_thread : public high_prio_thread_base {
    friend high_prio_thread_base;

public:
    high_prio_thread(event& evt) : evt_(evt) {}

private:
    void thread_function() {
        { // 1. wait/notify test
            evt_.wait();
            assert_that(clock::now(), is_eq(time_point(50_ms)));
        }

        { // 2. wait_for success and timeout test
            auto result = evt_.wait_for(50_ms);
            assert_that(result, is_eq(false));
            assert_that(clock::now(), is_eq(time_point(100_ms)));

            result = evt_.wait_for(100_ms);
            assert_that(result, is_eq(true));
            assert_that(clock::now(), is_eq(time_point(150_ms)));
        }

        { // 3. wait_until success and timeout test
            auto result = evt_.wait_until(time_point(200_ms));
            assert_that(result, is_eq(false));
            assert_that(clock::now(), is_eq(time_point(200_ms)));

            result = evt_.wait_until(time_point(300_ms));
            assert_that(result, is_eq(true));
            assert_that(clock::now(), is_eq(time_point(250_ms)));
        }

        { // 4. test assert on double wait
            evt_.wait();
        }
    }

    event& evt_;
};

class low_prio_thread;
using low_prio_thread_base = thread<low_prio_thread>;
class low_prio_thread : public low_prio_thread_base {
    friend low_prio_thread_base;

public:
    low_prio_thread(event& evt) : evt_(evt) {}

private:
    void thread_function() {
        { // 1. wait/notify test
            this_thread::sleep_for(50_ms);
            evt_.notify();
        }

        { // 2. wait_for success and timeout test
            this_thread::sleep_for(100_ms);
            evt_.notify();
        }

        { // 3. wait_for success and timeout test
            this_thread::sleep_for(100_ms);
            evt_.notify();
        }

        { // 4. test assert on double wait
            evt_.wait();
        }
    }

    event& evt_;
};

int main(int argv, char** argc) {
    start_test("event_test", argv, argc);

    static event evt;
    static high_prio_thread high(evt);
    static low_prio_thread low(evt);

    kernel::initialize(50_MHz, 100_Hz);

    kernel::set_error_handler([](kernel::error err) {
        assert_that(err, is_eq(kernel::error::rtos_assert));
        end_test();
    });

    high.start();
    low.start();

    kernel::start();
    return 0;
}
