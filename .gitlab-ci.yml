image: registry.gitlab.com/sat-polsl/software/docker-arm-toolchain/toolchain

stages:
  - build-static
  - build-dynamic
  - formatting
  - test-static
  - test-dynamic
  - quality

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - when: always

variables:
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  SONAR_OUTPUT_DIR: sonar-output

build-static:
  tags:
    - firmware
  stage: build-static
  interruptible: true
  before_script:
    - cmake -B build-static
          -D MCU=stm32h7
          -D TARGET_CORE=cortex-m7
          -D FLOAT_ABI=hard
          -D FREERTOS_MINIMAL_STACK_SIZE=1024
  script:
    - cmake --build build-static --target satos

build-dynamic:
  tags:
    - firmware
  stage: build-dynamic
  interruptible: true
  before_script:
    - cmake -B build-dynamic
      -D MCU=stm32h7
      -D TARGET_CORE=cortex-m7
      -D FLOAT_ABI=hard
      -D FREERTOS_MINIMAL_STACK_SIZE=1024
      -D SATOS_DYNAMIC_ALLOCATION=ON
  script:
    - cmake --build build-dynamic --target satos

format:
  stage: formatting
  interruptible: true
  script:
    - check_cpp_formatting

test-static:
  tags:
    - firmware
  stage: test-static
  interruptible: true
  cache:
    policy: pull-push
    key: "${CI_PIPELINE_ID}"
    paths:
      - test-static
      - sonar-static
  before_script:
    - cmake -B test-static
      -D MCU=stm32h7
      -D TARGET_CORE=cortex-m7
      -D FLOAT_ABI=hard
      -D FREERTOS_MINIMAL_STACK_SIZE=1024
  script:
    - build-wrapper-linux-x86-64 --out-dir sonar-static cmake --build test-static --target all
    - ctest --test-dir test-static --output-junit report.xml --output-on-failure
  artifacts:
    when: always
    reports:
      junit: test-static/report.xml

test-dynamic:
  tags:
    - firmware
  stage: test-dynamic
  interruptible: true
  cache:
    policy: pull-push
    key: "${CI_PIPELINE_ID}"
    paths:
      - test-static
      - sonar-static
      - test-dynamic
      - sonar-dynamic
  before_script:
    - cmake -B test-dynamic
      -D MCU=stm32h7
      -D TARGET_CORE=cortex-m7
      -D FLOAT_ABI=hard
      -D FREERTOS_MINIMAL_STACK_SIZE=1024
      -D SATOS_DYNAMIC_ALLOCATION=ON
  script:
    - build-wrapper-linux-x86-64 --out-dir sonar-dynamic cmake --build test-dynamic --target all
    - ctest --test-dir test-dynamic --output-junit report.xml --output-on-failure
  artifacts:
    when: always
    reports:
      junit: test-dynamic/report.xml

sonarcloud-check:
  tags:
    - firmware
  stage: quality
  interruptible: true
  cache:
    policy: pull
    key: "${CI_PIPELINE_ID}"
    paths:
      - test-static
      - sonar-static
      - test-dynamic
      - sonar-dynamic
  before_script:
    - sed -i "/^#/d" sonar-static/build-wrapper-dump.json
    - sed -i "/^#/d" sonar-dynamic/build-wrapper-dump.json
    - mkdir ${SONAR_OUTPUT_DIR}
    - >
      jq -s '{version: .[0].version, captures: (.[0].captures + .[1].captures)}' 
      sonar-static/build-wrapper-dump.json 
      sonar-dynamic/build-wrapper-dump.json > ${SONAR_OUTPUT_DIR}/build-wrapper-dump.json
  script:
    - >
      sonar-scanner
      -Dsonar.host.url="${SONAR_HOST_URL}"
      -Dsonar.token="${SONAR_TOKEN}"
      -Dproject.settings=.sonar.properties
