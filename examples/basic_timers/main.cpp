#include "satos/kernel.h"
#include "satos/timer.h"
#include "satext/units.h"

using namespace std::chrono_literals;

struct gpio {
    void toggle() { /*...*/
    }
};

satos::timer gpio_timer{100ms};

int main() {

    satos::kernel::initialize(100_MHz, 100_Hz);
    gpio_timer.register_callback([]() {
        static gpio output;
        static std::uint32_t timer_factor{};

        output.toggle();
        if (++timer_factor > 10) {
            timer_factor = 1;
        }
        gpio_timer.start(timer_factor * 100ms);
    });

    gpio_timer.start();

    satos::kernel::start();
    while (true) {}
}
