#include "satos/kernel.hpp"
#include "satos/thread.hpp"
#include "satos/mutex.hpp"
#include "satext/units.h"
#include "../examples/helpers.h"
#include <mutex>

using namespace std::chrono_literals;

struct resource {
    void access(){/*...*/};
};

class example_thread_mutex;
using example_thread_mutex_base = satos::thread<example_thread_mutex>;

class example_thread_mutex : public example_thread_mutex_base {
    friend example_thread_mutex_base;

public:
    explicit example_thread_mutex(resource& res, satos::mutex& mtx) :
        _shared_resource{res},
        _mtx{mtx} {}

private:
    [[noreturn]] void thread_function() {
        while (true) {
            _mtx.lock();
            _shared_resource.access();
            _mtx.unlock();
            satos::this_thread::sleep_for(100ms);
        }
    }

    resource& _shared_resource;
    satos::mutex& _mtx;
};

class example_thread_lock;
using example_thread_lock_base = satos::thread<example_thread_lock>;

class example_thread_lock : public example_thread_lock_base {
    friend example_thread_lock_base;

public:
    explicit example_thread_lock(resource& res, satos::mutex& mtx) :
        _shared_resource{res},
        _mtx{mtx} {}

private:
    [[noreturn]] void thread_function() {
        while (true) {
            {
                std::lock_guard lck{_mtx};
                _shared_resource.access();
            }
            satos::this_thread::sleep_for(100ms);
        }
    }

    resource& _shared_resource;
    satos::mutex& _mtx;
};

int main() {
    static satos::mutex mtx;
    static resource shared_resource;
    static auto mtx_threads = helpers::create_array<example_thread_mutex, 5>(shared_resource, mtx);
    static auto lock_threads = helpers::create_array<example_thread_lock, 5>(shared_resource, mtx);

    satos::kernel::initialize(100_MHz, 100_Hz);

    for (auto& t : mtx_threads) {
        t.start();
    }

    for (auto& t : lock_threads) {
        t.start();
    }

    satos::kernel::start();
    while (true) {}
}
