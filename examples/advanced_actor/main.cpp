#include "satos/kernel.h"
#include "satos/actor.h"
#include "satext/units.h"

/*
 *            --> B
 *           /      \
 *  ISR -> A         --> D
 *           \      /
 *            --> C
 */

struct msg_to_a {
    std::int32_t val;
};

struct msg_from_a {
    std::int32_t val;
};

struct msg_to_d {
    std::int32_t val;
};

class actor_a;
using actor_a_base = satos::actor<actor_a>;

class actor_a : public actor_a_base {
    friend actor_a_base;

private:
    void on_message(msg_to_a& m) {
        process(m.val);
        emit(msg_from_a{m.val});
    }

    void process(std::int32_t&) { /*...*/
    }
};

class actor_b;
using actor_b_base = satos::actor<actor_b>;

class actor_b : public actor_b_base {
    friend actor_b_base;

private:
    void on_message(msg_from_a& m) {
        process(m.val);
        emit(msg_to_d{m.val});
    }

    void process(std::int32_t&) {}
};

class actor_c;
using actor_c_base = satos::actor<actor_c>;

class actor_c : public actor_c_base {
    friend actor_c_base;

private:
    void on_message(msg_from_a& m) {
        process(m.val);
        emit(msg_to_d{m.val});
    }

    void process(std::int32_t&) { /*...*/
    }
};

class actor_d;
using actor_d_base = satos::actor<actor_d>;

class actor_d : public actor_d_base {
    friend actor_d_base;

private:
    void on_message(msg_to_d& m) { process(m.val); }

    void process(std::int32_t&) { /*...*/
    }
};

actor_a a;

void interrupt_handler() {
    using isr_msg = actor_a::actor_message<msg_to_a>;
    static isr_msg msg;
    static std::int32_t value{};
    msg = isr_msg{msg_to_a{value++}};
    a.send(&msg);
}

int main() {
    static actor_b b;
    static actor_c c;
    static actor_d d;

    satos::kernel::initialize(100_MHz, 100_Hz);

    a.connect<msg_from_a>(&b);
    a.connect<msg_from_a>(&c);
    b.connect<msg_to_d>(&d);
    c.connect<msg_to_d>(&d);

    a.start();
    b.start();
    c.start();
    d.start();

    satos::kernel::start();
    while (true) {}
}
