if (SATOS_TEST)
    CPMAddPackage(
        NAME semihosting
        GITLAB_REPOSITORY sat-polsl/software/semihosting
        VERSION 1.0.0
        OPTIONS
        "SEMIHOSTING_COVERAGE_FILTER satos"
    )

    CPMAddPackage("gh:fmtlib/fmt#9.1.0")
endif ()

CPMAddPackage("gl:sat-polsl/software/satext@1.0.0")

CPMAddPackage(
    NAME googletest
    GITHUB_REPOSITORY google/googletest
    GIT_TAG main
    OPTIONS
    "gtest_disable_pthreads ON"
    EXCLUDE_FROM_ALL YES
)

set_target_properties(gtest gmock gmock_main
    PROPERTIES
    CXX_STANDARD 20
    CXX_STANDARD_REQUIRED ON
    CXX_EXTENSIONS ON
    )

target_compile_definitions(gtest
    PRIVATE
    _POSIX_PATH_MAX=128
    )
