# SATOS

C++ wrapper for [FreeRTOS](https://www.freertos.org/).

[Project Wiki](https://sat-polsl.gitlab.io/wiki/satos/) includes setup guide and API documentation.
