set(TARGET satos_newlib)

add_library(${TARGET} STATIC)
add_library(satos::newlib ALIAS ${TARGET})

target_sources(${TARGET}
    PRIVATE
    src/syscalls.cpp
    )

target_compile_definitions(${TARGET}
    PRIVATE
    SATOS_DYNAMIC_ALLOCATION=$<IF:$<BOOL:${SATOS_DYNAMIC_ALLOCATION}>,1,0>
    )

target_link_libraries(${TARGET}
    PRIVATE
    freertos
    )

target_link_directories(${TARGET}
    INTERFACE
    ${CMAKE_CURRENT_SOURCE_DIR}
    )

target_link_options(${TARGET}
    INTERFACE
    LINKER:--undefined=_close
    LINKER:--undefined=_fstat
    LINKER:--undefined=_isatty
    LINKER:--undefined=_lseek
    LINKER:--undefined=_exit
    LINKER:--undefined=_kill
    LINKER:--undefined=_getpid
    LINKER:--undefined=_write
    LINKER:--undefined=_read
    LINKER:--undefined=_open
    LINKER:--undefined=_stat
    LINKER:--undefined=_link
    LINKER:--undefined=_unlink
    LINKER:--undefined=_wait
    LINKER:--undefined=_execve
    LINKER:--undefined=_fork
    LINKER:--undefined=_times
    LINKER:--undefined=_sbrk
    LINKER:--undefined=__malloc_lock
    LINKER:--undefined=__malloc_unlock
    $<$<BOOL:${SATOS_DYNAMIC_ALLOCATION}>:LINKER:--defsym=HEAP_SIZE=${SATOS_HEAP_SIZE}>
    )