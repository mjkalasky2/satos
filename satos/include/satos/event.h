#pragma once

#include "satos/thread.h"
#include "satos/clock.h"
#include "satos/concept/event.h"
#include "satos/critical_section.h"

namespace satos {

class event {
public:
    event() = default;
    event(const event&) = delete;
    event& operator=(const event&) = delete;
    event(event&&) noexcept = delete;
    event& operator=(event&&) noexcept = delete;
    ~event() = default;

    void notify();

    void wait();

    [[nodiscard]] bool wait_for(clock::duration timeout);

    [[nodiscard]] bool wait_until(clock::time_point sleep_time);

    template<typename F, typename... Args>
    void wait_after(F&& f, Args&&... args) {
        {
            critical_section c;
            kernel::expects(thread_ == nullptr);
            thread_ = this_thread::get_native_handle();
        }
        std::invoke(std::forward<F>(f), std::forward<Args>(args)...);
        wait(clock::to_ticks(clock::duration::max()));
        thread_ = nullptr;
    }

    template<typename F, typename... Args>
    [[nodiscard]] bool wait_for_after(clock::duration timeout, F&& f, Args&&... args) {
        {
            critical_section c;
            kernel::expects(thread_ == nullptr);
            thread_ = this_thread::get_native_handle();
        }
        std::invoke(std::forward<F>(f), std::forward<Args>(args)...);
        auto result = wait(clock::to_ticks(timeout));
        thread_ = nullptr;
        return result;
    }

    template<typename F, typename... Args>
    [[nodiscard]] bool wait_until_after(clock::time_point sleep_time, F&& f, Args&&... args) {
        {
            critical_section c;
            kernel::expects(thread_ == nullptr);
            thread_ = this_thread::get_native_handle();
        }
        auto now = clock::now();
        std::invoke(std::forward<F>(f), std::forward<Args>(args)...);
        auto result = wait(clock::to_ticks(sleep_time - now));
        thread_ = nullptr;
        return result;
    }

private:
    bool wait(std::uint32_t ticks);

    thread_native_handle thread_{nullptr};
};

static_assert(event_concept<event>);

} // namespace satos