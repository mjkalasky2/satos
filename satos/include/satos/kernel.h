#pragma once

#include <cstdint>
#include <array>
#include "satext/inplace_function.h"
#include "satos/port.h"

namespace satos {

class kernel {
public:
    enum class state : std::int32_t {
        inactive = 0,
        ready = 1,
        running = 2,
        locked = 3,
        suspended = 4,
        error = -1
    };

    enum class status : std::int32_t {
        ok = 0,
        error = -1,
        error_timeout = -2,
        error_resource = -3,
        error_parameter = -4,
        error_no_memory = -5,
        error_isr = -6
    };

    kernel(const kernel&) = delete;
    kernel& operator=(const kernel&) = delete;
    kernel(kernel&&) noexcept = delete;
    kernel& operator=(kernel&&) noexcept = delete;
    ~kernel() noexcept = default;

    enum class error : std::int32_t {
        nullptr_dereference = -1,
        out_of_range = -2,
        out_of_memory = -3,
        rtos_assert = -4
    };

    static status initialize(std::uint32_t cpu_hz, std::uint32_t tick_rate_hz);

    static status start();

    static void stop();

    static state get_state();

    static status lock();

    static status unlock();

    static std::uint32_t get_tick_count();

    static std::uint32_t get_tick_freq();

    static std::uint32_t get_tick_duration_ms();

    static void enter_critical();

    static void exit_critical();

    static void idle_handler();

    static void error_handler(kernel::error error);

    static void set_idle_handler(const satext::inplace_function<void()> handler);

    static void set_error_handler(const satext::inplace_function<void(kernel::error)> handler);

    static void isr_push();

    static std::int32_t isr_pop();

    static std::int32_t* get_isr_yield();

    static void expects(bool x);

private:
    state state_{state::inactive};

    satext::inplace_function<void(kernel::error)> error_handler_{
        [](kernel::error) { kernel::stop(); }};

    satext::inplace_function<void()> idle_handler_{[]() {}};

    std::uint32_t tick_duration_ms_{};

    std::array<std::int8_t, 10> isr_yield_stack_{};
    std::int8_t* isr_yield_stack_ptr_{isr_yield_stack_.data() + isr_yield_stack_.size()};
    std::int32_t current_isr_yield_{};

    kernel() = default;

    static kernel& get_instance();
};
} // namespace satos
