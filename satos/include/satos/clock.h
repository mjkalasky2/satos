#pragma once
#include "satos/port.h"
#include "satos/chrono.h"

namespace satos {

struct clock {
    using duration = satos::chrono::milliseconds;
    using rep = duration::rep;
    using period = duration::period;
    using time_point = std::chrono::time_point<clock>;
    static constexpr bool is_steady = true;

    static std::uint32_t tick_duration_ms();

    static time_point now() noexcept;

    static time_point from_now(duration duration) noexcept;

    static time_point next_time_point(duration period);

    static std::uint32_t to_ticks(duration duration);

    static std::uint32_t to_ticks(time_point time);
};
} // namespace satos
