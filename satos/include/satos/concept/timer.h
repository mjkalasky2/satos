#pragma once
#include <concepts>
#include "satos/clock.h"
#include "satext/inplace_function.h"
#include "satos/timer_autoreload.h"

namespace satos {
// clang-format off
template<typename T>
concept timer_concept = requires(T timer, clock::duration duration, autoreload reload,
                                 satext::inplace_function<void()> callback) {
    T(duration, reload);
    { timer.register_callback(callback) } -> std::same_as<void>;
    { timer.initialize() } -> std::same_as<void>;
    { timer.start() } -> std::same_as<void>;
    { timer.start(duration) } -> std::same_as<void>;
    { timer.stop() } -> std::same_as<void>;
    { timer.is_running() } -> std::same_as<bool>;
};
// clang-format on
}; // namespace satos
