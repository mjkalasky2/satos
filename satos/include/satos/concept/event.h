#pragma once
#include <concepts>
#include "satos/clock.h"

namespace satos {
// clang-format off
template<typename T>
concept event_concept = requires(T event, clock::duration duration, clock::time_point time_point) {
    { event.notify() } -> std::same_as<void>;
    { event.wait() } -> std::same_as<void>;
    { event.wait_for(duration) } -> std::same_as<bool>;
    { event.wait_until(time_point) } -> std::same_as<bool>;
};
// clang-format on
}; // namespace satos
