#pragma once

#include "satos/kernel.h"
#include "satos/thread.h"
#include "satos/mutex.h"
#include "satos/clock.h"
#include "satos/condition_variable_status.h"
#include <mutex>
#if SATOS_DYNAMIC_ALLOCATION
#include <list>
#endif

namespace satos {

namespace detail {
class condition_variable_base {
public:
    condition_variable_base(const condition_variable_base&) = delete;
    condition_variable_base& operator=(const condition_variable_base&) = delete;
    condition_variable_base(condition_variable_base&&) noexcept = delete;
    condition_variable_base& operator=(condition_variable_base&&) noexcept = delete;

protected:
    condition_variable_base() = default;
    ~condition_variable_base() noexcept = default;

    template<typename T>
    void wait_impl(std::unique_lock<satos::mutex>& lock) {
        if (!is_isr()) {
            thread_native_handle thread = this_thread::get_native_handle();
            auto derived = static_cast<T*>(this);
            kernel::lock();
            if (derived->push_back_thread(thread) == false) {
                kernel::unlock();
                kernel::error_handler(kernel::error::out_of_range);
                return;
            }
            kernel::unlock();

            atomic_unlock_and_suspend(lock, clock::to_ticks(clock::duration::max()));

            lock.lock();
        }
    }

    template<typename T, class predicate>
    void wait_impl(std::unique_lock<satos::mutex>& lock, predicate pred) {
        while (!pred()) {
            wait_impl<T>(lock);
        }
    }

    template<typename T>
    [[nodiscard]] cv_status wait_for_impl(std::unique_lock<satos::mutex>& lock,
                                          clock::duration rel_time) {
        return wait_until_impl<T>(lock, clock::now() + rel_time);
    }

    template<typename T, class predicate>
    [[nodiscard]] bool
    wait_for_impl(std::unique_lock<satos::mutex>& lock, clock::duration rel_time, predicate pred) {
        while (!pred()) {
            if (wait_for_impl<T>(lock, rel_time) == cv_status::timeout) {
                return pred();
            }
        }
        return true;
    }

    template<typename T>
    [[nodiscard]] cv_status wait_until_impl(std::unique_lock<satos::mutex>& lock,
                                            clock::time_point timeout_time) {
        cv_status status{};
        if (!is_isr()) {
            thread_native_handle thread = this_thread::get_native_handle();
            auto derived = static_cast<T*>(this);
            kernel::lock();
            if (derived->push_back_thread(thread) == false) {
                kernel::unlock();
                kernel::error_handler(kernel::error::out_of_range);
                return status;
            }
            kernel::unlock();

            auto ticks = clock::to_ticks(timeout_time);
            auto now = kernel::get_tick_count();
            atomic_unlock_and_suspend(lock, ticks - now);

            kernel::lock();
            if (auto it =
                    std::find(derived->thread_queue_.begin(), derived->thread_queue_.end(), thread);
                it != derived->thread_queue_.end()) {
                derived->erase_thread(it);
                status = cv_status::timeout;
            } else {
                status = cv_status::no_timeout;
            }
            kernel::unlock();

            lock.lock();
        }
        return status;
    }

    template<typename T, class predicate>
    [[nodiscard]] bool wait_until_impl(std::unique_lock<satos::mutex>& lock,
                                       clock::time_point timeout_time,
                                       predicate pred) {
        while (!pred()) {
            if (wait_until_impl<T>(lock, timeout_time) == cv_status::timeout) {
                return pred();
            }
        }
        return true;
    }

    template<typename T>
    void notify_all_impl() {
        auto derived = static_cast<T*>(this);
        if (derived == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
            return;
        }
        if (is_isr()) {
            while (!derived->thread_queue_empty()) {
                thread_native_handle thread = derived->thread_queue_.front();
                derived->pop_front_thread();

                notify_thread_from_isr(thread, kernel::get_isr_yield());
            }
        } else {
            kernel::lock();
            while (!derived->thread_queue_empty()) {
                thread_native_handle thread = derived->thread_queue_.front();
                derived->pop_front_thread();
                notify_thread(thread);
            }
            kernel::unlock();
        }
    }

    template<typename T>
    void notify_one_impl() {
        auto derived = static_cast<T*>(this);
        if (derived == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
            return;
        }
        if (is_isr()) {
            thread_native_handle thread{nullptr};

            if (!derived->thread_queue_empty()) {
                thread = derived->thread_queue_.front();
                derived->pop_front_thread();
            }

            if (thread != nullptr) {
                notify_thread_from_isr(thread, kernel::get_isr_yield());
            }
        } else {
            kernel::lock();
            thread_native_handle thread{nullptr};

            if (!derived->thread_queue_empty()) {
                thread = derived->thread_queue_.front();
                derived->pop_front_thread();
            }

            kernel::unlock();

            if (thread != nullptr) {
                notify_thread(thread);
            }
        }
    }

private:
    static void atomic_unlock_and_suspend(std::unique_lock<satos::mutex>& lock,
                                          std::uint32_t ticks_to_wait);

    static void notify_thread(thread_native_handle thread);

    static void notify_thread_from_isr(thread_native_handle thread, std::int32_t* yield);

    static void yield_from_isr(std::int32_t yield);
};

template<size_t max_threads_waiting = 1>
class condition_variable_static : public condition_variable_base {
    friend condition_variable_base;

public:
    condition_variable_static() = default;
    condition_variable_static(const condition_variable_static&) = delete;
    condition_variable_static& operator=(const condition_variable_static&) = delete;
    condition_variable_static(condition_variable_static&&) noexcept = delete;
    condition_variable_static& operator=(condition_variable_static&&) noexcept = delete;
    ~condition_variable_static() noexcept = default;

    void wait(std::unique_lock<satos::mutex>& lock) {
        condition_variable_base::wait_impl<condition_variable_static>(lock);
    }

    template<class predicate>
    void wait(std::unique_lock<satos::mutex>& lock, predicate pred) {
        condition_variable_base::wait_impl<condition_variable_static>(lock, pred);
    }

    [[nodiscard]] cv_status wait_for(std::unique_lock<satos::mutex>& lock,
                                     clock::duration rel_time) {
        return condition_variable_base::wait_for_impl<condition_variable_static>(lock, rel_time);
    }

    template<class predicate>
    [[nodiscard]] bool
    wait_for(std::unique_lock<satos::mutex>& lock, clock::duration rel_time, predicate pred) {
        return condition_variable_base::wait_for_impl<condition_variable_static>(lock, rel_time,
                                                                                 pred);
    }

    [[nodiscard]] cv_status wait_until(std::unique_lock<satos::mutex>& lock,
                                       clock::time_point timeout_time) {
        return condition_variable_base::wait_until_impl<condition_variable_static>(lock,
                                                                                   timeout_time);
    }

    template<class predicate>
    [[nodiscard]] bool wait_until(std::unique_lock<satos::mutex>& lock,
                                  clock::time_point timeout_time,
                                  predicate pred) {
        return condition_variable_base::wait_until_impl<condition_variable_static>(
            lock, timeout_time, pred);
    }

    void notify_one() { condition_variable_base::notify_one_impl<condition_variable_static>(); }

    void notify_all() { condition_variable_base::notify_all_impl<condition_variable_static>(); }

private:
    std::array<thread_native_handle, max_threads_waiting> thread_queue_{};
    thread_native_handle* thread_queue_end_{thread_queue_.begin()};

    bool thread_queue_empty() { return thread_queue_.begin() == thread_queue_end_; }

    bool push_back_thread(thread_native_handle thread) {
        if (thread_queue_end_ != thread_queue_.end()) {
            *thread_queue_end_ = thread;
            thread_queue_end_++;
            return true;
        } else {
            return false;
        }
    }

    void pop_front_thread() {
        if (!thread_queue_empty()) {
            std::copy(thread_queue_.begin() + 1, thread_queue_end_, thread_queue_.begin());
            thread_queue_end_--;
            *thread_queue_end_ = nullptr;
        }
    }

    void erase_thread(thread_native_handle* it) {
        if (!thread_queue_empty()) {
            std::copy(it + 1, thread_queue_end_, it);
            thread_queue_end_--;
            *thread_queue_end_ = nullptr;
        }
    }
};

#if SATOS_DYNAMIC_ALLOCATION
class condition_variable : public condition_variable_base {
    friend condition_variable_base;

public:
    condition_variable() = default;
    condition_variable(const condition_variable&) = delete;
    condition_variable& operator=(const condition_variable&) = delete;
    condition_variable(condition_variable&&) noexcept = delete;
    condition_variable& operator=(condition_variable&&) noexcept = delete;
    ~condition_variable() noexcept = default;

    void wait(std::unique_lock<satos::mutex>& lock) {
        condition_variable_base::wait_impl<condition_variable>(lock);
    }

    template<class predicate>
    void wait(std::unique_lock<satos::mutex>& lock, predicate pred) {
        condition_variable_base::wait_impl<condition_variable>(lock, pred);
    }

    [[nodiscard]] cv_status wait_for(std::unique_lock<satos::mutex>& lock,
                                     clock::duration rel_time) {
        return condition_variable_base::wait_for_impl<condition_variable>(lock, rel_time);
    }

    template<class predicate>
    [[nodiscard]] bool
    wait_for(std::unique_lock<satos::mutex>& lock, clock::duration rel_time, predicate pred) {
        return condition_variable_base::wait_for_impl<condition_variable>(lock, rel_time, pred);
    }

    [[nodiscard]] cv_status wait_until(std::unique_lock<satos::mutex>& lock,
                                       clock::time_point timeout_time) {
        return condition_variable_base::wait_until_impl<condition_variable>(lock, timeout_time);
    }

    template<class predicate>
    [[nodiscard]] bool wait_until(std::unique_lock<satos::mutex>& lock,
                                  clock::time_point timeout_time,
                                  predicate pred) {
        return condition_variable_base::wait_until_impl<condition_variable>(lock, timeout_time,
                                                                            pred);
    }

    void notify_one() { condition_variable_base::notify_one_impl<condition_variable>(); }

    void notify_all() { condition_variable_base::notify_all_impl<condition_variable>(); }

private:
    std::list<thread_native_handle> thread_queue_;

    bool thread_queue_empty() { return thread_queue_.empty(); }

    bool push_back_thread(thread_native_handle thread) {
        thread_queue_.push_back(thread);
        return true;
    }

    void pop_front_thread() { thread_queue_.pop_front(); }

    void erase_thread(const std::list<thread_native_handle>::iterator& it) {
        thread_queue_.erase(it);
    }
};
#endif // SATOS_DYNAMIC_ALLOCATION
} // namespace detail

#if SATOS_DYNAMIC_ALLOCATION
using condition_variable = detail::condition_variable;
template<size_t max_threads_waiting = 1>
using condition_variable_static = detail::condition_variable_static<max_threads_waiting>;
#else
template<size_t max_threads_waiting = 1>
using condition_variable = detail::condition_variable_static<max_threads_waiting>;
#endif // SATOS_DYNAMIC_ALLOCATION
} // namespace satos
