#pragma once
#include <cstdint>

namespace satos::detail {
#ifdef __ARM_ARCH_6M__
constexpr bool arm_arch_6m = true;
#else
constexpr bool arm_arch_6m = false;
#endif
#ifdef __ARM_ARCH_7M__
constexpr bool arm_arch_7m = true;
#else
constexpr bool arm_arch_7m = false;
#endif
#ifdef __ARM_ARCH_7EM__
constexpr bool arm_arch_7em = true;
#else
constexpr bool arm_arch_7em = false;
#endif
#ifdef __ARM_ARCH_8M_MAIN__
constexpr bool arm_arch_8m_main = true;
#else
constexpr bool arm_arch_8m_main = false;
#endif

bool is_isr_masked();
bool is_isr_mode();
bool is_isr();

} // namespace satos::detail
