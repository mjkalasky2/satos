#include "satos/clock.h"
#include "satos/kernel.h"

namespace satos {

std::uint32_t clock::tick_duration_ms() { return kernel::get_tick_duration_ms(); }

clock::time_point clock::from_now(clock::duration duration) noexcept {
    if (duration == duration::max()) {
        return time_point::max();
    } else {
        return now() + duration;
    }
}

clock::time_point clock::now() noexcept {
    return time_point(chrono::milliseconds(kernel::get_tick_count() * tick_duration_ms()));
}

std::uint32_t clock::to_ticks(clock::duration duration) {
    if (duration.count() == std::numeric_limits<std::uint32_t>::max()) {
        return std::numeric_limits<std::uint32_t>::max();
    }
    return static_cast<std::uint32_t>(duration / chrono::milliseconds(tick_duration_ms()));
}

std::uint32_t clock::to_ticks(clock::time_point time) {
    if (time.time_since_epoch().count() == std::numeric_limits<std::uint32_t>::max()) {
        return std::numeric_limits<std::uint32_t>::max();
    }
    return static_cast<std::uint32_t>(time.time_since_epoch() /
                                      chrono::milliseconds(tick_duration_ms()));
}

clock::time_point clock::next_time_point(clock::duration period) {
    return clock::time_point{(now().time_since_epoch() / period + 1) * period};
}

} // namespace satos
