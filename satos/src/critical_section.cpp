#include "satos/critical_section.h"
#include "satos/kernel.h"

namespace satos {

critical_section::critical_section() { kernel::enter_critical(); }

critical_section::~critical_section() { kernel::exit_critical(); }

} // namespace satos
