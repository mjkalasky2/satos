#include "satos/isr.h"
#include "satos/kernel.h"
#include "FreeRTOS.h"

satos::isr::isr() { kernel::isr_push(); }

satos::isr::~isr() { portYIELD_FROM_ISR(kernel::isr_pop()); }
