#include "satos/timer.h"
#include "FreeRTOS.h"
#include "timers.h"

namespace satos::detail {
class timer_base::impl {
public:
    impl(timer_base* timer_base,
         chrono::milliseconds timer_period,
         autoreload reload,
         TimerCallbackFunction_t callback);
    impl(const impl&) = delete;
    impl& operator=(const impl&) = delete;
    impl(impl&&) noexcept = delete;
    impl& operator=(impl&&) noexcept = delete;
    ~impl() noexcept;

    void initialize();

    void start();

    void start(std::uint32_t timer_period);

    void stop();

    [[nodiscard]] bool is_running();

private:
    timer_base* timer_base_{nullptr};
    chrono::milliseconds timer_period_{};
    autoreload autoreload_{};
    TimerCallbackFunction_t callback_{nullptr};
    TimerHandle_t timer_handle_{nullptr};
    StaticTimer_t timer_control_block_{};
};

timer_base::impl::impl(timer_base* timer_base,
                       chrono::milliseconds timer_period,
                       autoreload reload,
                       TimerCallbackFunction_t callback) :
    timer_base_{timer_base},
    timer_period_{timer_period},
    autoreload_{reload},
    callback_{callback} {}

timer_base::impl::~impl() noexcept {
    if (!is_isr()) {
        xTimerDelete(timer_handle_, 0);
    }
}

void timer_base::impl::initialize() {
    if (timer_handle_ == nullptr && !is_isr()) {
        timer_handle_ =
            xTimerCreateStatic(nullptr, clock::to_ticks(timer_period_),
                               static_cast<std::int32_t>(satext::to_underlying_type(autoreload_)),
                               timer_base_, callback_, &timer_control_block_);
    }
}

void timer_base::impl::start() {
    if (timer_handle_ == nullptr) {
        initialize();
    }
    if (timer_handle_ == nullptr) {
        kernel::error_handler(kernel::error::nullptr_dereference);
        return;
    }
    if (is_isr()) {
        xTimerStartFromISR(timer_handle_, kernel::get_isr_yield());
    } else {
        xTimerStart(timer_handle_, 0);
    }
}

void timer_base::impl::stop() {
    if (timer_handle_ == nullptr) {
        kernel::error_handler(kernel::error::nullptr_dereference);
        return;
    }
    if (is_isr()) {
        xTimerStopFromISR(timer_handle_, kernel::get_isr_yield());
    } else {
        xTimerStop(timer_handle_, 0);
    }
}

void timer_base::impl::start(std::uint32_t timer_period) {
    if (timer_handle_ == nullptr) {
        initialize();
    }
    if (timer_handle_ == nullptr) {
        kernel::error_handler(kernel::error::nullptr_dereference);
        return;
    }
    if (is_isr()) {
        xTimerChangePeriodFromISR(timer_handle_, timer_period, kernel::get_isr_yield());
    } else {
        xTimerChangePeriod(timer_handle_, timer_period, 0);
    }
}

bool timer_base::impl::is_running() {
    if (!is_isr()) {
        if (timer_handle_ == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
            return false;
        }
        return xTimerIsTimerActive(timer_handle_) != pdFALSE;
    } else {
        return false;
    }
}

timer_base::timer_base(chrono::milliseconds timer_period,
                       autoreload reload,
                       timer_base::callback_ptr callback_function) :
    impl_{*reinterpret_cast<impl*>(&impl_storage_)},
    impl_storage_{} {
    static_assert(sizeof(impl) == sizeof(impl_storage_));
    static_assert(std::alignment_of<impl>::value == alignof(impl_storage_));
    new (&impl_storage_) impl(this, timer_period, reload,
                              reinterpret_cast<TimerCallbackFunction_t>(callback_function));
}

timer_base::~timer_base() { reinterpret_cast<impl*>(&impl_storage_)->~impl(); }

void* timer_base::get_timer_id(void* handle) {
    return pvTimerGetTimerID(reinterpret_cast<tmrTimerControl*>(handle));
}
void timer_base::initialize() { impl_.initialize(); }

void timer_base::start() { impl_.start(); }

void timer_base::start(clock::duration timer_period) { impl_.start(clock::to_ticks(timer_period)); }

void timer_base::stop() { impl_.stop(); }

bool timer_base::is_running() { return impl_.is_running(); }
} // namespace satos::detail
