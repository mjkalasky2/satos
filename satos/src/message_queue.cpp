#include "satos/message_queue.h"
#include "FreeRTOS.h"
#include "queue.h"

namespace satos::detail {
class message_queue_base::impl {
public:
    impl(uint8_t* data, size_t queue_size, size_t item_size);
    impl(const impl&) = delete;
    impl& operator=(const impl&) = delete;
    impl(impl&&) noexcept = delete;
    impl& operator=(impl&&) noexcept = delete;
    ~impl() noexcept;

    [[nodiscard]] bool push(const void* in, std::uint32_t timeout);

    [[nodiscard]] bool pull(void* out, std::uint32_t timeout);

    [[nodiscard]] size_t size() const;

    void clear();

private:
    StaticQueue_t queue_control_block_{};
    QueueHandle_t queue_handle_{nullptr};
};

message_queue_base::impl::impl(uint8_t* data, size_t queue_size, size_t item_size) {
    if (!is_isr()) {
        queue_handle_ = xQueueCreateStatic(queue_size, item_size, data, &queue_control_block_);
        if constexpr (configQUEUE_REGISTRY_SIZE > 0) {
            vQueueAddToRegistry(queue_handle_, nullptr);
        }
    }
}

message_queue_base::impl::~impl() noexcept {
    if (!is_isr()) {
        if constexpr (configQUEUE_REGISTRY_SIZE > 0) {
            vQueueUnregisterQueue(queue_handle_);
        }
        vQueueDelete(queue_handle_);
    }
}

bool message_queue_base::impl::push(const void* const in, std::uint32_t timeout) {
    if (queue_handle_ == nullptr) {
        kernel::error_handler(kernel::error::nullptr_dereference);
        return false;
    }
    if (is_isr()) {
        if (timeout != 0) {
            return false;
        }
        return xQueueSendToBackFromISR(queue_handle_, in, kernel::get_isr_yield()) == pdTRUE;
    } else {
        return xQueueSendToBack(queue_handle_, in, timeout) == pdPASS;
    }
}

bool message_queue_base::impl::pull(void* const out, std::uint32_t timeout) {
    if (queue_handle_ == nullptr) {
        kernel::error_handler(kernel::error::nullptr_dereference);
        return false;
    }
    if (is_isr()) {
        if (timeout != 0) {
            return false;
        }
        return xQueueReceiveFromISR(queue_handle_, out, kernel::get_isr_yield()) == pdPASS;
    } else {
        return xQueueReceive(queue_handle_, out, timeout) == pdPASS;
    }
}

size_t message_queue_base::impl::size() const {
    if (queue_handle_ == nullptr) {
        kernel::error_handler(kernel::error::nullptr_dereference);
        return 0;
    }
    if (is_isr()) {
        return uxQueueMessagesWaitingFromISR(queue_handle_);
    } else {
        return uxQueueMessagesWaiting(queue_handle_);
    }
}

void message_queue_base::impl::clear() {
    if (!is_isr()) {
        if (queue_handle_ == nullptr) {
            kernel::error_handler(kernel::error::nullptr_dereference);
        } else {
            xQueueReset(queue_handle_);
        }
    }
}

message_queue_base::message_queue_base(uint8_t* data, size_t queue_size, size_t item_size) :
    impl_{*reinterpret_cast<impl*>(&impl_storage_)},
    impl_storage_{} {
    static_assert(sizeof(impl) == sizeof(impl_storage_));
    static_assert(std::alignment_of<impl>::value == alignof(impl_storage_));
    new (&impl_storage_) impl(data, queue_size, item_size);
}

message_queue_base::~message_queue_base() noexcept {
    reinterpret_cast<impl*>(&impl_storage_)->~impl();
}

bool message_queue_base::push(const void* const in, std::uint32_t timeout) {
    return impl_.push(in, timeout);
}

bool message_queue_base::pull(void* const out, std::uint32_t timeout) {
    return impl_.pull(out, timeout);
}

size_t message_queue_base::size() const { return impl_.size(); }

void message_queue_base::clear() { impl_.clear(); }

} // namespace satos::detail
