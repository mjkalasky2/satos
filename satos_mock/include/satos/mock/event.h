#pragma once

#include "gmock/gmock.h"
#include "satos/concept/event.h"
#include "satos/clock.h"
#include <vector>

namespace satos::mock {
struct event {
    event() { events.push_back(this); }

    MOCK_METHOD(void, notify, ());
    MOCK_METHOD(void, wait, ());
    MOCK_METHOD(bool, wait_for, (const satos::clock::duration&));
    MOCK_METHOD(bool, wait_until, (const satos::clock::time_point&));

    inline static std::vector<event*> events{};
};

static_assert(satos::event_concept<satos::mock::event>);
} // namespace satos::mock
