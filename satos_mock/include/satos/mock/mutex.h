#pragma once

#include "gmock/gmock.h"
#include "satos/concept/mutex.h"
#include "satos/clock.h"
#include <vector>

namespace satos::mock {
struct mutex {
    mutex() { mutexes.push_back(this); }

    MOCK_METHOD(void, lock, ());
    MOCK_METHOD(void, unlock, ());
    MOCK_METHOD(bool, try_lock, ());
    MOCK_METHOD(bool, try_lock_for, (const satos::clock::duration&));
    MOCK_METHOD(bool, try_lock_until, (const satos::clock::time_point&));

    inline static std::vector<mutex*> mutexes{};
};

static_assert(satos::mutex_concept<satos::mock::mutex>);
} // namespace satos::mock
