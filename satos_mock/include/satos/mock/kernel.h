#pragma once
#include "gmock/gmock.h"
#include "satos/kernel.h"

namespace satos::mock {
class kernel {
public:
    kernel(const kernel&) = delete;
    kernel& operator=(const kernel&) = delete;
    kernel(kernel&&) noexcept = delete;
    kernel& operator=(kernel&&) noexcept = delete;
    ~kernel() noexcept = default;

    MOCK_METHOD(satos::kernel::status, initialize, (std::uint32_t, std::uint32_t));
    MOCK_METHOD(satos::kernel::status, start, ());
    MOCK_METHOD(void, stop, ());
    MOCK_METHOD(satos::kernel::state, get_state, ());
    MOCK_METHOD(satos::kernel::status, lock, ());
    MOCK_METHOD(satos::kernel::status, unlock, ());
    MOCK_METHOD(std::uint32_t, get_tick_count, ());
    MOCK_METHOD(std::uint32_t, get_tick_freq, ());
    MOCK_METHOD(std::uint32_t, get_tick_duration_ms, ());
    MOCK_METHOD(void, enter_critical, ());
    MOCK_METHOD(void, exit_critical, ());
    MOCK_METHOD(void, idle_handler, ());
    MOCK_METHOD(void, error_handler, (satos::kernel::error));
    MOCK_METHOD(void, set_idle_handler, (const satext::inplace_function<void()>));
    MOCK_METHOD(void,
                set_error_handler,
                (const satext::inplace_function<void(satos::kernel::error)>));
    MOCK_METHOD(void, isr_push, ());
    MOCK_METHOD(std::int32_t, isr_pop, ());
    MOCK_METHOD(std::int32_t*, get_isr_yield, ());
    MOCK_METHOD(void, expects, (bool));

    static kernel& instance();

    static void reset();

private:
    kernel() = default;
};
} // namespace satos::mock
