#pragma once

#include "gmock/gmock.h"
#include "satos/concept/semaphore.h"
#include "satos/kernel.h"
#include <vector>
#include <optional>

namespace satos::mock {
struct flags {
    flags() { flags_vector.push_back(this); }

    MOCK_METHOD(std::optional<std::uint32_t>, wait, (std::uint32_t, bool, satos::clock::duration));
    MOCK_METHOD(std::optional<std::uint32_t>,
                wait_for_all,
                (std::uint32_t, bool, satos::clock::duration));
    MOCK_METHOD(std::optional<std::uint32_t>,
                wait,
                (std::uint32_t, bool, satos::clock::time_point));
    MOCK_METHOD(std::optional<std::uint32_t>,
                wait_for_all,
                (std::uint32_t, bool, satos::clock::time_point));
    MOCK_METHOD(std::optional<std::uint32_t>, set, (std::uint32_t));
    MOCK_METHOD(std::optional<std::uint32_t>, clear, (std::uint32_t));
    MOCK_METHOD(std::optional<std::uint32_t>, get, ());
    MOCK_METHOD(std::optional<std::uint32_t>,
                sync,
                (std::uint32_t, std::uint32_t, satos::clock::duration));

    inline static std::vector<flags*> flags_vector{};
};

static_assert(satos::flags_concept<satos::mock::flags>);
} // namespace satos::mock
