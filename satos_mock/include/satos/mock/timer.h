#pragma once

#include "gmock/gmock.h"
#include "satos/concept/timer.h"
#include "satext/inplace_function.h"
#include <vector>

namespace satos::mock {
struct timer {
    timer(satos::clock::duration, autoreload) { timers.push_back(this); }

    MOCK_METHOD(void, register_callback_inner, (satext::inplace_function<void()>));
    MOCK_METHOD(void, initialize, ());
    MOCK_METHOD(void, start_void, ());
    MOCK_METHOD(void, start_duration, (satos::clock::duration));
    MOCK_METHOD(void, stop, ());
    MOCK_METHOD(bool, is_running, ());

    void start() { start_void(); }

    void start(satos::clock::duration duration) { start_duration(duration); }

    void register_callback(satext::inplace_function<void()> callback) {
        callbacks.push_back(callback);
        register_callback_inner(callback);
    }

    inline static std::vector<timer*> timers{};

    std::vector<satext::inplace_function<void()>> callbacks{};
};

static_assert(satos::timer_concept<satos::mock::timer>);
} // namespace satos::mock
